#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:15:01 2018

@author: game
"""
"""Archivo revizado y optimiado, pendiente test"""



class MQTT():
    """Clase para emular el comportamiento del protocolo MQTT CLIENTE usando la red
       Lora, esta no implementa todos los metodos necesario por el protocolo
       MQTT

       """

    def __init__(self,callback=None):
        self.set_callback(callback)  ##Establece la función que se llama cuando llega un msm por lora

    def set_lora(self,lora):
        self.lora=lora

    def set_callback(self, f):
        """Metodo para establecer la función que se llama cuando llega un dato
           por el dispositivo Lora"""
        self.callback = f

    def wait_msg(self, msm):
        """Función que se llama por defecto cuando llega un dato por el
           dispositivo Lora"""
        msm = msm.spliter(" ")
        payload = msm[1]
        topic = msm[0]
        if self.callback != None:
            self.callback(topic,payload)
        else:
            print(msm)


    def publish(self, topic, payload):
        """Funcion para publicar datos siguiendo el formato topic - payload"""
        msm = topic+ " "+payload
        #print(msm)
        self.lora.enviar(msm)
