#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 16 12:59:34 2018

@author: game
"""
from Lora.Lora import Lora
from Lora.LoraWan import EndDevice
from MQTT import MQTT

##importacion de sensores
from DHT22 import DHT22   #Linea autogenerada
##------------------------
import time
from gps import GpsNeo6
from config import Clave_Red,id

class Sonda():

    def __init__(self,debug=False):
        self.debug = debug
        self.mqtt = MQTT(self.recepcion)
        self.gps  = GpsNeo6(2,9600,diff=2)
        self.LoraWan=EndDevice(callrx=self.mqtt.wait_msg,passw=Clave_Red)

        self.mqtt.set_lora(self.LoraWan)
        ###lineas autogenerada
        self.topicos={'SENSOR_1_TEMPERATURA': '/LA_MORITA/UVAS/NODO_1/SENSOR_1/Temp/Dth22',
                     'SENSOR_1_HUMEDAD': '/LA_MORITA/UVAS/NODO_1/SENSOR_1/Hum/Dth22'}

        #Este diccionario guarda los objetos que definen los sensores de las sondas
        self.sensores = {'SENSOR_1': DHT22('SENSOR_1_',22)}    #linea autogenerada
        #En este diccionario se guardan los datos leidos de cada uno de los sensores
        self.datos ={'SENSOR_1':None}              #linea autogenerada

        #Este diccionario asocioa topicos de solicitud con las funciones que se deben
        #ejecutar para cumplir con la tarea requerida
        self.topic_function={'/LA_MORITA/UVAS/NODO_1/time':self.send_time,
                       '/LA_MORITA/UVAS/NODO_1/date':self.send_date,
                       '/LA_MORITA/UVAS/NODO_1/posicion':self.send_posicion,
                       '/LA_MORITA/UVAS/NODO_1/altura':self.send_altura}

        #Diccionario para asocciar una operacion con un topico de respuesta
        self.par_topic={'time': '/LA_MORITA/UVAS/NODO_1/time',
                       'date': '/LA_MORITA/UVAS/NODO_1/date',
                       'posicion': '/LA_MORITA/UVAS/NODO_1/posicion',
                       'altura': '/LA_MORITA/UVAS/NODO_1/altura'}
        ###-----------------------------

    def send_time(self):
        """Metodo para enviar la hora al broker"""
        self.time(self.par_topic['time'])

    def send_date(self):
        """Metodo para enviar la fecha al broker, se piensa usar para
           avisarle al broker cuando se cambio de dia"""
        self.date(self.par_topic['date'])

    def send_posicion(self):
        """Metodo para enviar la posicion al broker, se piesa usar para
           avisarle al broker si la sonda esta siendo desplazada"""
        self.posicion(self.par_topic['posicion'])

    def send_altura(self):
        """Metodo para enviar la altura al broker"""
        self.altura(self.par_topic['altura'])

    def send_sensores(self):
        """Metodo para enviar los datos de los diferentes sensores al broker"""
        for sensor,dicct in self.datos.items():
            for topico, value in dicct.items():
                if topico != 'time':
                    self.mqtt.publish(self.topicos[topico],str(value)+'-'+str(self.datos[sensor]['time']))
                    time.sleep(1)

    def read_sensores(self):
        """Lee los todos los sensores que estan conectados en la sonda"""
        for name_sensor, objsensor in self.sensores.items():
            self.datos[name_sensor]=objsensor.readData()
            self.gps.get_time()
            self.datos[name_sensor]['time']=self.gps.hora
        if self.debug:
            print(self.datos)

    def recepcion(self,topic,payload):
        """Funcion para atender lo que se recive, se atienden solicitud de datos
           especificos por parte del broker"""
        for topico,funcion in self.topic_function.items():
            if topico == topic:
                funcion(topic)

    def time(self,topic):
        """Lee y envia hora al broker"""
        t=self.gps.get_time()
        self.mqtt.publish(topic,t)
        if self.debug:
            print("Hora "+topic+t)

    def date(self,topic):
        """Lee y envia fecha al broker"""
        d=self.gps.get_date()
        self.mqtt.publish(topic,d)
        if self.debug:
            print("Fecha "+topic+d)

    def posicion(self,topic):
        """Lee y envia posicion al broker"""
        d=self.gps.get_posicion()
        self.mqtt.publish(topic,d)
        if self.debug:
            print("Posicion "+topic+d)

    def altura(self,topic):
        """Lee y envia altura al broker"""
        d=self.gps.get_altura()
        self.mqtt.publish(topic,d)
        if self.debug:
            print("Altura "+topic+d)
