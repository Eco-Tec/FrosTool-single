#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Lora.Lora import Lora
import time
from config import id
from machine import Timer

class EndDevice(Lora):

    def __init__(self,callrx,passw):
        Lora.__init__(self,Modo='Tx',callrx=self.rx_endDevice,passw=passw)
        self.back=callrx
        self.id = self.crear_id(id)
        self.espera=0  #numero de veces que se intento para establecer la conexion
        self.conexion=False #Determina si el dispositivo fue autorizado a transmitir en la red
        self.conectando = False
        self.intentos=0
        self.contador_pack =0
        self.Timewindows = 4
        self.Timer = Timer(0)              # create a timer object using timer 4
        self.conectar(3)


    def rx_endDevice(self,msm):
        """Metodo utilizado para gestionar los datos recividos por broker
        verifica que el paquete recivido contenga un id valida, para responderle
        de manera afimativa al cliente y retorna el payload al programa que lo solicite"""
        a = self.read_packet(msm)
        if a['id']== self.id:
            if a['counter']== 0:
                if a['payload']=='True':
                    self.conexion = True
            elif a['couter'] >0 and self.conexion and a['couter']<99:
                self.back(a['payload'])

    def conectar(self,intentos):
        """MEtodo utilizado para inicializar la conexion y deteminar si el
        dispositivo esta autorizado a en esta red
        intentos determina el numero de intentos te autenticacion"""
        self.espera=0
        self.intentos=intentos
        self.set_callrx(self.rx_endDevice)
        self.conectando=True
        while self.conexion==False and  self.conectando:
            print('Intentando autenticar....')
            self.contador_pack=0
            pack=self.crear_packet("id")
            self.set_RxTx('Tx')
            time.sleep(2)
            self.send(pack)
            self.set_RxTx('Rx')
            if self.espera>self.intentos:
                self.conexion=False
                self.conectando=False
                print('Broker no responde')
            self.espera+=self.espera+1

    def ventana(self,timer):
        self.espera+=1
        if self.espera>self.Timewindows:
            self.conexion=False
            self.conectando=False

    def crear_id(self,id):
        a=len(id)
        if a<=16:
            c=16-a
            id = id+" "*c
            return id
        else:
            print('Id supera los 16 caracteres')

    def crear_payload(self,payload):
        a=len(payload)
        if a<=112:
            c=112-a
            payload = payload+' '*c
            return payload
        else:
            print('Payload supera la loguitud')

    def crear_counter(self):
        counter=str(self.contador_pack)
        a = len(counter)
        if a<=3:
            c=3-a
            counter = (' '*c)+counter
            return counter
        else:
            print('El contador de paquetes excede la longuitud')

    def crear_packet(self,msm):
        payload=self.crear_payload(msm)
        counter =self.crear_counter()
        return self.id+payload+counter

    def read_packet(self,msm):
        a=len(msm)
        id = msm[0:16]
        payload = msm[16:a-3]
        counter = int(msm[a-3:])
        return {'id':id,'payload':payload,'counter':counter}

    def enviar(self,msm):
        if self.conexion:
            pack=self.crear_packet(msm)
            self.send(pack)

    def send(self,pack):
        self.contador_pack+=1
        print(pack)
        self.println(pack)
