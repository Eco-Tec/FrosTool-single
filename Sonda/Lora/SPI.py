#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2015-2018 Mayer Analytics Ltd.
#
# This file is part of pySX127x.
#
# pySX127x is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# pySX127x is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You can be released from the requirements of the license by obtaining a commercial license. Such a license is
# mandatory as soon as you develop commercial activities involving pySX127x without disclosing the source code of your
# own applications, or shipping pySX127x with a closed source product.
#
# You should have received a copy of the GNU General Public License along with pySX127.  If not, see
# <http://www.gnu.org/licenses/>.


from machine import SPI as SPI_DEV
from machine import Pin as GPIO

class SPI():
    LORA_CS = 18
    LORA_SCK = 5
    LORA_MOSI = 27
    LORA_MISO = 19

    def __init__(self,hw=-1,baud=10000000):
        try:
            self.PIN_SS=GPIO(SPI.LORA_CS, GPIO.OUT)
            self.spi = SPI_DEV(-1, baudrate = 10000000, polarity = 0, phase = 0,
                  bits = 8, firstbit = SPI_DEV.MSB,sck = GPIO(self.LORA_SCK, GPIO.OUT,
                  GPIO.PULL_DOWN),mosi = GPIO(self.LORA_MOSI, GPIO.OUT, GPIO.PULL_UP),
                  miso = GPIO(self.LORA_MISO, GPIO.IN, GPIO.PULL_UP))
        except:
            if self.spi:
                self.spi.deinit()
            raise


    def xfer (self,value):
        response =bytearray(1)
        out =[]
        self.spi.init()
        self.PIN_SS.value(0)
        for i in value:
            self.spi.write_readinto(bytes([i]), response)
            out.append((response[0]))
        self.PIN_SS.value(1)
        self.spi.deinit()
        return out

    def transfer(self, address, value=0x00):
        self.spi.init()
        response = bytearray(1)
        self.spi.init()
        self.PIN_SS.value(0)
        self.spi.write(bytes([address]))
        self.spi.write_readinto(bytes([value]), response)
        self.PIN_SS.value(1)
        self.spi.deinit()
        return response
