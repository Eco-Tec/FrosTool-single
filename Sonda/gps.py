#/usr/bin/python3
#-*- coding: utf-8 -*-
"""Pendiente revicion para optimizar codigo, falta la parte de capturar
   la velocidad para determinar si la sonda esta siendo desplazada sin 
   autorizacion"""
from machine import UART
import time

class GpsNeo6():
    """
        class de gestion de modulo GPS NEO 6M
        """

    
    def __init__(self,port=2,baudio=9600,diff=1,utf=-5):
        """
           Se requiere el puerto, velocidad de lectura y zona 
           horaria UTF
            """
        self.port = UART(port,baudio )  # UART 2, PIN_TX 17
        self.utf = utf
        self.tabCode=["GPVTG","GPGGA","GPGSA","GPGLL","GPRMC","GPGSV"]
        self.nsatelites=""
        self.latitud=""
        self.date=""
        self.longitud=""
        self.hora=""
        self.altitud=""
        self.signal = 0
        
    def __del__(self):
        """
            on ferme le port a la destruction de l'objet
            """
        self.port.close()
        
    def fecha(self,l,f):
        """Toma la cadena del la fecha y las separa y ajusta el cambia de
        dia por la zona horaria UTF"""
        d=(int(l[0:1])*10)+int(l[1:2])
        if d< abs(self.utf):
            if f[0:2]=='01':
                self.date='31'+'/'+f[2:4]+'/20'+f[4:]
            else:
                self.date=str(int(f[0:2])-1)+'/'+f[2:4]+'/20'+f[4:]
        else:
                self.date=f[0:2]+'/'+f[2:4]+'/20'+f[4:]        
        
    def hora_utf(self,l):
        """Toma la cadena y la separa en hh:mm:ss.ss, se hace el ajuste por 
           la zona horaria
           """
        #if l[0:1]=='0':
        d=(int(l[0:1])*10)+int(l[1:2])
        if d< abs(self.utf):
            
            t=(24+d)+self.utf
            # hh           mm            ss   
            self.hora =str(t)+':'+l[2:4]+':'+l[4:]
        elif d==abs(self.utf):
            # hh           mm            ss   
            self.hora ='00:'+l[2:4]+':'+l[4:]
        else:
            # hh           mm            ss   
            self.hora =str(d+self.utf)+':'+l[2:4]+':'+l[4:]
        #else:
            # hh           mm            ss             
        #    self.hora =str(int(l[0:2])+self.utf)+':'+l[2:4]+':'+l[4:]
            
    def pos_latidud(self,l,d):
        if l[0]=='0':
             #     grados        minutos            segundos
            self.latitud=l[1:2]+'°'+l[2:]+chr(39)+chr(34)+" "+d
        else:
            self.latitud=l[0:2]+'°'+l[2:]+chr(39)+chr(34)+" "+d
        #print(self.latitud)
            
    def pos_longitud(self,l,d):
        if l[0]=='0':
            #     grados         minutos           segundos
            self.longitud=l[1:3]+'°'+l[3:]+chr(39)+" "+d
        else:
            self.longitud=l[0:3]+'°'+l[3:]+chr(39)+" "+d

    def command_gpgga(self,l):
        """Analiza todos los espacios del comando GPGGA"""
        l=l.split(',')
        try:
            if l[6] != "0":
                self.pos_latidud(l[2],l[3])  
                self.pos_longitud(l[4],l[5])
                self.nsatelites=l[7]
                self.altitud=l[9]+" "+l[10]
        except:
            print ("Comando GPGGA invalido")
                        
    def command_gprmc(self,l):
        """
            Analiza todos los espacios del comnando GPRMC
        """           
        l=l.split(',')
        try:
            if l[2]=='A':
                self.hora_utf(l[1])
        except:
            print ("Comando GPRMC invalido")
            
    def get_posicion(self):
        """Evalua la posicion en latitud y longitud, esta operación necesita 
           un tiempo minimo de 2 segundos para ser llamada nuevamente"""
        t=True
        while t:
            l=str(self.port.readline())
            time.sleep(2)
            if "GPRMC" in l:
                l=l.split(',')
                try:
                    if l[2]=="A":
                        self.pos_latidud(l[3],l[4])
                        self.pos_longitud(l[5],l[6])
                        t=False
                except:
                    self.signal=self.signal+1
                    if self.signal==10:
                        t=False
                        self.signal=0
                        print("No hay conexion con satelite")
                    pass
            elif "GPGGA"in l:
                l=l.split(',')
                try:
                    if l[6] != "0":
                        self.pos_latidud(l[2],l[3])
                        self.pos_longitud(l[4],l[5])
                        t=False
                except:
                    self.signal=self.signal+1
                    if self.signal==10:
                        t=False
                        self.signal=0
                        print("No hay conexion con satelite")
                    pass
            elif "GPGLL"in l:
                l=l.split(',')
                try:
                    if l[6]=='A':
                        self.pos_latidud(l[1],l[2])
                        self.pos_longitud(l[3],l[4])
                        t=False
                except:
                    self.signal=self.signal+1
                    if self.signal==10:
                        t=False
                        self.signal=0
                        print("No hay conexion con satelite")
    
    def get_altura(self):
        """Evalua la altura, esta operacion necesita minimo 2 segundos para volver
           a ser llamada"""
        t=True
        while t:
            l=str(self.port.readline())
            #print(l)
            time.sleep(1)
            if "GPGGA" in l:
                l=l.split(',')
                #print(l)
                try:
                    if l[6] != "0":
                        self.altitud=l[9]+" "+l[10]
                        t=False
                    else:
                        #print('cas')
                        self.signal=self.signal+1
                        if self.signal==10:
                            t=False
                            self.signal=0
                            print("No hay conexion con satelite")
                except:
                    self.signal=self.signal+1
                    if self.signal==10:
                        t=False
                        self.signal=0
                        print("No hay conexion con satelite")
                    pass
            
    def get_date(self):
        """Evalua la fecha """
        t=True
        while t:
            l=str(self.port.readline())
            if "GPRMC" in l:
                l=l.split(',')
                try:
                    if l[2]=='A':
                        self.fecha(l[1],l[9])
                        t=False
                    elif l[2]=='V':
                        self.fecha(l[1],l[9])
                        t=False
                        print("No hay conexion con satelite")
                except:
                    self.signal=self.signal+1
                    if self.signal==10:
                        t=False
                        self.signal=0
                        print("No hay conexion con satelite")
                    pass
            
    def get_time(self):
        """Evalua la hora, se verifico su correcto funcionamiento con lecturas
           de la hora cada 0.5 segundos"""
        t=True
        while t:
            l=str(self.port.readline())
            #print(l)
            if "GPRMC" in l:
                l=l.split(',')
                try:
                    self.hora_utf(l[1])
                    t=False
                except:
                    self.signal=self.signal+1
                    if self.signal==10:
                        t=False
                        self.signal=0
                        print("No hay conexion con satelite")
                    pass
            elif "GPGGA"in l:
                l=l.split(',')
                try:
                    self.hora_utf(l[1])
                    t=False
                except:
                    self.signal=self.signal+1
                    if self.signal==10:
                        t=False
                        self.signal=0
                        print("No hay conexion con satelite")
                    