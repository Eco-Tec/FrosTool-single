#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 11 21:11:11 2018

@author: game
"""

from .driver import *
from .Board import BOARD	
from .Register import*


class Lora(Sx127x):
    BOARD.setup()
    BOARD.reset_sx()
    def __init__(self, verbose=False,freq=868.1,pa_select=1,factor=1,word=0x34,callrx=None):
        super(Lora, self).__init__(verbose)
        self.set_mode(MODE.SLEEP)
        self.set_dio_mapping([0] * 6)  #interrupcion por rx
        self.set_freq(freq)
        self.set_pa_select(pa_select)
        self.set_spreading_factor(factor)
        self.set_sync_word(word)
        self.set_rx_crc(True)
        self.rx=callrx
        self.single = False
        #print(self)
        #self.save_config()
        
        
    def save_config(self):
        f=open("config.txt","w")
        f.write(self.__str__())
        f.close()
        
    def on_rx_done(self):
        BOARD.led_on()
        #print("\nRxDone")
        self.clear_irq_flags(rx_done=1)
        payload = self.read_payload(nocheck=True)
        if self.rx != None:
            self.rx(bytes(payload).decode())
        else:
            print(bytes(payload).decode())
        self.set_mode(MODE.SLEEP)
        self.reset_ptr_rx()
        BOARD.led_off()
        self.set_mode(MODE.RXCONT)
        
    def on_tx_done(self,msm):
        self.set_mode(MODE.STDBY)
        #self.clear_irq_flags(tx_done=1)
        if self.single:
            print
            sys.exit(0)
        BOARD.led_off()
        self.println(msm)
        while not self.get_irq_flags()['tx_done']:
            pass
        BOARD.led_on()
        self.set_mode(MODE.TX)
