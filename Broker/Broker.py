#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:57:14 2018

@author: game
"""
 
#from MQTT import MQTT
from Nodo import Nodo
#from Archivos import Archivos

from config import*

class Broker(object):
    
    def __init__(self):
        self.name_broker =CULTIVO["NAME_FINCA"]+'/'+CULTIVO["TIPO_CULTIVO"]
        print(self.name_broker)
        #self.csv = Archivos()
        self.archivos = {}
        ##Establecer los topicos de los sensores de cada una de las sondas 
        self.set_sondas()
        
     #   self.generate_topic()
        
        self.list_sondas= {}
        
    def set_sondas(self):
        for i, j in LIST_SENSOR.items():
            n=Nodo(self.name_broker,j)
        
    def generate_sondas(self):
        f = "/" + CULTIVO["TIPO_CULTIVO"]
        
        for i, m in LIST_SENSOR.items(): #recorre el diccionario list_sensor
            for j, k in m.items():       #recorre los sensonres de los nodos
                for d, l in k.items():   #recorre los topicos y varibles de los sensores
                    topic = f+ "/"+i +'/' +j+'/'+l[0]
                    self.list_sonda[i]=topic
                    
                    

        
    def generate_topic(self):
        c = 0
        f = "/" + CULTIVO["TIPO_CULTIVO"]
        for i in LIST_SENSOR:
            # print("a"+i)
            for m in LIST_SENSOR[i]:
               # print(m)
                if "SENSOR" in m:
                    for n in LIST_SENSOR[i][m]:
                        topic = f + \
                            str(LIST_SENSOR[i][m][n][0]) + "/" + i + "/" + m
                        self.list_topic["topic_" + str(c)] = topic
                        name = str(LIST_SENSOR[i][m][n][1]) + "/" + i + "/" + m
                        name = re.sub("/", "_", name)
                        self.add_file(
                            name, topic, LIST_SENSOR[i][m][n][1], "," + LIST_SENSOR[i]["LOCATION"], i, m)
                        c = c + 1

    def add_file(self, name, topic, var, location, nodo, sensor):
        """Crea un archivo nuevo y lo agrega al diccionario """
        name = self.ruta + str(name) + "_" + \
            time.strftime("%d_%m_%y") + "_" + ".csv"
        self.archivos[topic] = name
        # print(topic)
        self.csv.crear_file(
            name, BROKER["NUMBER_BROKER"], nodo, sensor, var)

