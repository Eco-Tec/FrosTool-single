#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
##https://www.elespectador.com/noticias/actualidad/mida-su-coeficiente-intelectual-con-este-quiz-articulo-817724?fbclid=IwAR0j6RgyIhmLElqnEpZncNpuT5dvxkAXhXTlrJJR2-Uw1y0jYFg_AI9WXjE
Created on Sat Sep 15 15:01:57 2018

@author: game
"""

import re
import time
import os

##########----------    Class   ----------##########
__author__ = "Marlon Moreno, Fabian A. Salamanca F."
__copyright__ = "Copyright 2017, Eco-Tec"
__credits__ = ["Fabian A. Salamanca F, Marlon Mauricio Moreno"]
__license__ = "GPL V3.0"
__version__ = "1.4.1"
__maintainer__ = __author__
__email__ = "fabian.salamanca@gmail.com"

class Archivos ():
    def __init__(self):
        self.plantilla = None  # open("plantilla.txt",'r')
        self.file = None

    def crear_file(self, name_file, broker, ubicacion, nodo, sensor, var,altura):
        self.plantilla = open(os.getcwd()+"/plantillas/plantilla.txt", 'r')
        self.file = open(name_file, 'w')
        self.encabezado(broker, ubicacion, sensor, nodo, var,altura)
        self.file.close()
        self.plantilla.close()
        
        
    def copiar(self, origen, destino):
        org = open(origen,'r')
        des = open(destino,'w')
        linea = org.readline()
        des.write(linea)
        while linea != "":
            linea = org.readline()
            des.write(linea)
        org.close()
        des.close()
        
    
        
    def read_linea(self, file,numero_linea):
        n=0
        d=""
        arc =open(file,'r')
        for i in arc:
            if n==numero_linea:
                d=arc
                break
            else:
                n=n+1
        
        arc.close()
        if d!= "":
            return d
        else:
            return 'Linea  no encontrada ....'       
        
    def write_linea(self, file, numero_linea, linea):
        """Escribe una linea especifica del archivo, borra el contenido 
           inicial de la linea"""
        n=0
        archivo =open (file, '+')
        for i in archivo:
            if n == numero_linea:
                archivo.write(linea)
                break
            else:
                n=n+1
        if n> numero_linea:
            for i in range(n,numero_linaea):
                archivo.write("")    
            archivo.write(linea)
        archivo.close()
            
        
                
                
            
