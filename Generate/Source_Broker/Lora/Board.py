""" Defines the BOARD class that contains the board pin mappings and RF module HF/LF info. """
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2015-2018 Mayer Analytics Ltd.
#
# This file is part of pySX127x.
#
# pySX127x is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# pySX127x is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You can be released from the requirements of the license by obtaining a commercial license. Such a license is
# mandatory as soon as you develop commercial activities involving pySX127x without disclosing the source code of your
# own applications, or shipping pySX127x with a closed source product.
#
# You should have received a copy of the GNU General Public License along with pySX127.  If not, see
# <http://www.gnu.org/licenses/>.

from machine import Pin as GPIO
import time
from .SPI import SPI


class BOARD:
    """ Board initialisation/teardown and pin configuration is kept here.
        Also, information about the RF module is kept here.
        This is the Raspberry Pi board with one LED and a modtronix inAir9B.
    """
    # Note that the BCOM numbering for the GPIOs is used.
    DIO0 = 26  		
    DIO1 = None   	
    DIO2 = None    
    DIO3 = None
    LED  = 2
    
    PIN_DIO0 = None
    PIN_DIO1 = None
    PIN_DIO2 = None
    PIN_DIO3 = None
    PIN_LED = None
    PIN_RESET = None

    LORA_RESET = 14

    

    # The spi object is kept here
    spi = None
    
    # tell pySX127x here whether the attached RF module uses low-band (RF*_LF pins) or high-band (RF*_HF pins).
    # low band (called band 1&2) are 137-175 and 410-525
    # high band (called band 3) is 862-1020
    low_band = True

    @staticmethod
    def setup():
        """ Configure the Raspberry GPIOs
        :rtype : None
        """
        # LED
        BOARD.PIN_LED=GPIO(BOARD.LED, GPIO.OUT)
        #PIN_LED=GPIO.init(LORA_CS, GPIO.OUT)
        BOARD.PIN_LED.value(0)
        BOARD.PIN_RESET = GPIO(BOARD.LORA_RESET, GPIO.OUT)
        BOARD.PIN_RESET.value (1)
        # DIOx
        BOARD.PIN_DIO0 = GPIO(BOARD.DIO0, GPIO.IN, GPIO.PULL_DOWN)
        #PIN_DIO1 = GPIO(BOARD.DIO1, GPIO.IN, pull=GPIO.PULL_DOWN)
        #PIN_DIO2 = GPIO(BOARD.DIO2, GPIO.IN, pull=GPIO.PULL_DOWN)
        #PIN_DIO3 = GPIO(BOARD.DIO3, GPIO.IN, pull=GPIO.PULL_DOWN)
        # blink 2 times to signal the board is set up
        BOARD.blink(.1, 2)
        
    @staticmethod
    def reset_sx():
        BOARD.PIN_RESET.value(0)
        time.sleep_us(11)
        BOARD.PIN_RESET.value(1)
        time.sleep_ms(6)

    @staticmethod
    def SpiDev():
        BOARD.spi = SPI()
        return BOARD.spi

    @staticmethod
    def teardown():
        """ Cleanup GPIO and SpiDev """
        #GPIO.cleanup()
        BOARD.spi.spi.deinit()

    @staticmethod
    def add_event_detect(dio_number, callback):
        """ Wraps around the GPIO.add_event_detect function
        :param dio_number: DIO pin 0...5
        :param callback: The function to call when the DIO triggers an IRQ.
        :return: None
        """
        #GPIO.irq(callback=callback,GPIO.IRQ_RISING,dio_number)
        pass

    @staticmethod
    def add_events(cb_dio0):
        BOARD.PIN_DIO0.irq(handler = cb_dio0,trigger = GPIO.IRQ_RISING)
        #PIN_DIO1.irq(cb_dio1,GPIO.IRQ_RISING)
        #PIN_DIO2.irq(cb_dio2,GPIO.IRQ_RISING)
        #PIN_DIO3.irq(cb_dio3,GPIO.IRQ_RISING)

    @staticmethod
    def led_on(value=1):
        """ Switch the proto shields LED
        :param value: 0/1 for off/on. Default is 1.
        :return: value
        :rtype : int
        """
        BOARD.PIN_LED.value(value)
        return value

    @staticmethod
    def led_off():
        """ Switch LED off
        :return: 0
        """
        BOARD.PIN_LED.value(0)
        return 0

    @staticmethod
    def blink(time_sec, n_blink):
        if n_blink == 0:
            return
        BOARD.led_on()
        for i in range(n_blink):
            time.sleep(time_sec)
            BOARD.led_off()
            time.sleep(time_sec)
            BOARD.led_on()
        BOARD.led_off()

