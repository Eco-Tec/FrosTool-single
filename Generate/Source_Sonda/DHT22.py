#/usr/bin/python3
#-*- coding: utf-8 -*-


"""Archivo revizado y optimizado"""

import dht
import machine


class DHT22():
    "Clase referente solo al sensor DHT22 (Pines, topicos, lectura de datos)"

    def __init__(self, name,pin_dht=22):
        #nombre del sensor
        self.temperatura = name+'TEMPERATURA'
        self.humedad = name+'HUMEDAD'
        self.pin_DHT = pin_dht
        self.s_dht = dht.DHT22(machine.Pin(self.pin_DHT))

    def readData(self):
        "Obtiene de los sensores los datos de Temperatura/Humedad"
        try:
            self.s_dht.measure()
            return({self.temperatura: self.s_dht.temperature(), self.humedad: self.s_dht.humidity()})
        except Exception as e:
            print("No fue posible la lectura de Datos error: "+ str(e))