"""Archivo en el que se especifican las caracteristicas especificas del cultivo"""


##Colocar la clase que describe el formato de los datos de los diferentes sensores
##usados en el proyecto.
from .type_sensor import*


##Se especifican las caracterisiticas especificas del cultivi

##tipo de cultivo
##nombre de la finca
##localización 

CULTIVO = {"TIPO_CULTIVO": "UVAS", "NAME_FINCA": "LA_MORITA", "LOCATION": "5.7180354, -72.9244577"}


##Se especifican las caracteristicas de los sensores que hay en cada uno de los
##nodo del proyecto, se debe crear un diccionario para cada nodo

NODO_1 = {"SENSOR_1": DTH22,"SENSOR_3": DTH22}
NODO_2 = {"SENSOR_1": DTH22,"SENSOR_5": DTH22}


##Se deben especificar los nodos que se utilizaran en el proyecto y se a
##asocia la descripción de sensores que compone cada uno de los nodos del 
##cultivo
LIST_SENSOR = {"NODO_1": NODO_1,"NODO_2":NODO_2}






