#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:57:14 2018

@author: game
"""
 
#from MQTT import MQTT
from .Nodo import Nodo
from .broker import broker
from .Archivos import Archivos
from .db import*

from .config import*

import os
import shutil
import datetime


class Generador(object):
    """Clase principal del proyecto para generar el codigo fuente correspondiente
       para el broker y cada una de las sondas que componen el proyecto"""
    def __init__(self, destino):
        self.tipo = destino
        if self.tipo =="Broker":
            self.csv = Archivos()
            ##Nombre sel broker
            self.name_broker =CULTIVO["NAME_FINCA"]+'/'+CULTIVO["TIPO_CULTIVO"]
            ##ruta para ubicar los archivos que se correran en el broker
            self.path =os.getcwd()+"/Proyecto/Broker"
            self.remove_dir(self.path)
            os.mkdir(self.path)
            ##Ruta de la ubicacíon del codigo fuente usado para autogenerar el proyecto
            self.source = os.getcwd()+'/Source_Broker'
            ##Diccionario para guardar los nodos que componene el proyecto
            self.nodos ={}
            self.name_files = {}
            self.datetime={}
            self.Broker = broker()
            self.db = db(self.path,self.fecha(),self.name_broker)
            self.set_sondas()
            
            os.mkdir(self.path+'/pantillas')
            self.generate_file()
            #self.set_topic_datetime()
            #self.db =db(self.path,)
        elif self.tipo == "Sonda":
            pass
            return
        
        
        ##Establecer los topicos de los sensores de cada una de las sondas 
        
        #print(self.nodos)
        #print(self.name_files)
        
    def remove_dir(self, path):
        try:
            shutil.rmtree(path)
        except:
            pass     
        
    def fecha(self):
        x = datetime.datetime.now()
        f=""
        f =str(x.year)+'-'+str(x.month)+'-'+str(x.day)
        return str(f)
                        
    def set_sondas(self):
        """Metodo llamado para determinar las caracteristicas de las sondas
           que se van a usar en el cultivo"""
        for i, j in LIST_SENSOR.items():
            n=Nodo(self.name_broker,j,i)
            self.nodos[i]=n.sensores
            self.name_files[i]=n.name_file
            
    def set_topic_datetime(self):
        m={}
        for i,j in LIST_SENSOR.items():
            for d, k in j.items():
                g=dict (
                time =self.name_broker+'/'+i+'/'+d+'/time',
                date =self.name_broker+'/'+i+'/'+d+'/date',
                posicion =self.name_broker+'/'+i+'/'+d+'/posicion',
                altura =self.name_broker+'/'+i+'/'+d+'/altura')
                m[d]=g
            self.datetime[i]=m
        #print(self.datetime)
        
    def generate_file(self):
        """Genera los archivos plantilla para todos los sensores de las 
           sondas."""
        ruta =self.path+ '/pantillas/'
        for i, m in self.name_files.items(): #recorre el diccionario 
            for n,k in m.items():
               for d, u in k.items():
                   #print(u)
                   self.csv.crear_file(ruta+u+'.csv',self.name_broker,"",i,n,d,"")
                    
                    

