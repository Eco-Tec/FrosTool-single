#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 16:12:42 2018

@author: game
"""

class Nodo(object):
    
    def __init__(self, name_sonda, sensors, nodo):
        self.sensores ={}
        self.name_file = {}
        self.add_sensors(name_sonda,sensors,nodo)
        
    def __del__(self):
        del(self.sensores)
        
    def add_sensors(self,name,sensor,nodo):
        for i, j in sensor.items():
            h={}
            r={}
            for d, e in j.items():
                topic=name+'/'+nodo+'/'+i+e[0]
                h[e[1]] = topic
                r[e[1]] = e[0].replace('/','_')
            self.name_file[i]=r
            self.sensores[i]=h
            del(h)