#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 21:57:34 2018

Este archivo se usara para agregar datos a los achivos csv y crear nuevos csv

@author: game
"""
from .Archivos import Archivos
import os



class db():
    """Clase para crear la base de datos que usara el broker, esta hace parte
       del proyecto encargado de la autogeneración de los proyectos"""
    
    def __init__(self,path, fecha, name_broker):
        print("Creando base de datos del broker")
        self.path =path+"/db"  #ruta donde se guarda la base de datos
        self.fecha=fecha
        self.name_broker =name_broker
        os.mkdir(self.path)
        self.csv = Archivos()
        
        self.date = None
        self.posicion = None
        self.altura = None
        self.init_db()
        
        
    def init_db (self):
        """Metodo para inicializar la base de datos por primera vez, requiere
           los nombre de los sensores, variables de cada uno y la fecha inicial
           """
        from temporal import variables
        self.variables = variables()
        for name_nodo, dicci in self.variables.nodos.items():
            os.mkdir(self.path+'/'+name_nodo)
            for name_sensor, lista in dicci.items():
                os.mkdir(self.path+'/'+name_nodo+'/'+name_sensor)
                for variables in lista:
                    os.mkdir(self.path+'/'+name_nodo+'/'+name_sensor+'/'+variables)
                    path=self.path+'/'+name_nodo+'/'+name_sensor+'/'+variables
                    self.generate_file(path,name_nodo,name_sensor,variables)
                    
    def generate_file(self, path,name_nodo,name_sensor,variable):
        """Genera los archivos iniciales de la base de datos del broker."""
        ruta =path
        self.csv.crear_file(ruta+'/'+self.fecha +'.csv',self.name_broker,self.posicion,name_nodo,name_sensor,variable,self.altura)
                    