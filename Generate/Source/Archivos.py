#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 15:01:57 2018

@author: game
"""

import re
import time
import os

##########----------    Class   ----------##########
__author__ = "Marlon Moreno, Fabian A. Salamanca F."
__copyright__ = "Copyright 2017, Eco-Tec"
__credits__ = ["Fabian A. Salamanca F, Marlon Mauricio Moreno"]
__license__ = "GPL V3.0"
__version__ = "1.4.1"
__maintainer__ = __author__
__email__ = "fabian.salamanca@gmail.com"

class Archivos ():
    def __init__(self):
        self.plantilla = None  # open("plantilla.txt",'r')
        self.file = None

    def crear_file(self, name_file, broker, ubicacion, nodo, sensor, var,altura):
        self.plantilla = open(os.getcwd()+"/Source/plantilla.txt", 'r')
        self.file = open(name_file, 'w')
        self.encabezado(broker, ubicacion, sensor, nodo, var,altura)
        self.file.close()
        self.plantilla.close()

    def encabezado(self, broker=None, ubicacion=None, sensor=None, nodo=None,
                   var=None, altura=None):
        linea = self.plantilla.readline()
        while linea != "":

            if linea == "# Copyrigh\n":
                #m=re.sub("\s", __copyright__ ,linea)
                self.file.write("# " + __copyright__ + "\n")
            elif linea == "# License\n":
                # print("# "+__copyright__+"\n")
                m = re.sub("\n", "  ," + __license__ + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# Version\n":
                # print("# "+__copyright__+"\n")
                m = re.sub("\n", "  ," + __version__ + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# Generado por el Broker\n":
                m = re.sub("\n", "  ," + broker + "\n", linea)
                # print(m)
                self.file.write(m)
            #elif linea == "# fecha\n":
            #    m = re.sub("\n", "  ," +
            #               time.strftime("%d/%m/%y") + "\n", linea)
                # print(m)
            #    self.file.write(m)
            #elif linea == "# UBICACION\n":
            #    m = re.sub("\n", "  " + ubicacion + "\n", linea)
            #    # print(m)
            #    self.file.write(m)
            elif linea == "# SENSOR\n":
                m = re.sub("\n", "  ," + sensor + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# NODO\n":
                m = re.sub("\n", "  ," + nodo + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# ALTURA":
                m = re.sub("\n", "  ," + altura + "\n", linea)
                # print(m)
                self.file.write(m)
                
            elif linea == "TIME,var\n":
                m = re.sub("var", var + "\n", linea)
                self.file.write(m)
            else:
                # print(linea)
                self.file.write(linea)
            linea = self.plantilla.readline()