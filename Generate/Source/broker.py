#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 23:59:33 2018

@author: game
"""

from .config import*
import os

class broker():
    
    def __init__(self):
        print('Generando proyecto para broker')
        self.temporal = open(os.getcwd()+'/temporal.py', 'w')
        self.init_temporal()
        self.sensores()
        
    def init_temporal(self):
        self.temporal.write("class variables(): \n")
        self.temporal.write("    def __init__(self): \n")
        
    def sensores(self):
        """Genera el diccionario sensores para la clase Broker del proyecto"""
        name ="self.sensores_"
        s = {}
        a=""
        b=""
        nodos = "self.nodos ={"
        n = len(LIST_SENSOR)
        d=1
        r=1
        entrada =""
        for name_nodo,dicci in LIST_SENSOR.items():
            dd =len(dicci)
            r=1
            entrada = ""
            for name_sensor, dicc_sensor in dicci.items():
                l = []
                for name_variable, lista in dicc_sensor.items():
                    l.append(lista[1])
                if r<dd:
                    entrada = entrada + "'"+name_sensor +"'" +':'+str(l)+','
                    r=r+1
                else:
                    r=r+1
                    entrada = entrada + "'"+name_sensor+"'" +':'+str(l)
            if d<n:
                a = a+"'"+name_nodo+"'"+':'+name+name_nodo+','
                b = b+'        '+name+name_nodo+'={'+entrada + '}\n'
                d=d+1
            else:
                a = a+"'"+name_nodo+"'"+':'+name+name_nodo
                b = b+'        '+name+name_nodo+'={'+entrada + '}\n'
                d=d+1
        s = nodos+a+'}'
        self.temporal.write(b)
        self.temporal.write('        '+s)
        self.temporal.close()
            
        