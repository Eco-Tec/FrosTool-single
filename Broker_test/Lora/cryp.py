#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:15:01 2018

@author: game
"""
"""Archivo revizado y optimiado, pendiente test"""

from ucryptolib import aes

class cryp():

    def __init__(self,passw):
        self.passw=self.llenar(passw)
        IV = bytearray(len(self.passw))
        self.aes =aes(self.passw,1,IV)

    def llenar(self,msm):
        #print(msm)
        a=len(msm)
        b=(a/16)
        c=int(b)*16
        if a<=16:
            t=16-a
            return msm+' '*t
        elif b>1:
            if a > c:
                t=(c+16)-a

                return msm+' '*t
            else:
                return msm

    def encriptar(self,msm):
        msm=self.llenar(msm)
        a=bytearray(len(msm))
        a=self.aes.encrypt(msm)
        return a

    def desencriptar(self,msm):
        msm=self.llenar(msm)
        a=bytearray(len(msm))
        a= self.aes.decrypt(msm)
        return a
