#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 11 21:11:11 2018

@author: game
"""

from .driver import *
from .Board import BOARD
from .Register import*
from .cryp import cryp

class Lora(Sx127x):
    BOARD.setup()
    BOARD.reset_sx()
    def __init__(self, Modo = "Rx",verbose=False,freq=869,pa_select=1,factor=7,
                 word=0x34,callrx=None,preamble =8,bw=BW.BW125,coding_rate =CODING_RATE.CR4_5,
                 ocp =100,passw=None):
        super(Lora, self).__init__(verbose)
        if passw!=None:
            try:
                self.aes=cryp(passw)
            except:
                self.aes=False

        self.set_mode(MODE.SLEEP)
        self.set_freq(freq)
        self.set_preamble(preamble)
        self.set_pa_select(pa_select)
        self.set_spreading_factor(factor)
        self.set_sync_word(word)
        self.set_bw(bw)
        self.set_coding_rate(coding_rate)
        self.set_ocp_trim(ocp)
        self.rx=callrx
        self.single = False
        #print(self)
        self.set_RxTx(Modo)
        self.aux = self.mode
        self.save_config()

    def set_RxTx(self,modo):
        if modo == "Rx":
            self.set_dio_mapping([0]*6) #interrupcion por Rx
            self.modo = "Rx"
            self.reset_ptr_rx()
            self.set_mode(MODE.RXCONT)
        else:
            self.set_dio_mapping([1,0,0,0,0,0]) #interrupcion por tx
            self.modo ="Tx"
            self.set_mode(MODE.TX)

    def save_config(self):
        self.set_mode(MODE.SLEEP)
        f=open("config.txt","w")
        f.write(self.__str__())
        #print(self.__str__())
        f.close()
        self.set_mode(self.aux)

    def on_rx_done(self):
        """Metodo llamado cada vez que se recive un mensaje exitoso"""
        BOARD.led_on()
        self.clear_irq_flags(rx_done=1)
        payload = self.read_payload(nocheck=True)
        if self.aes!= False:
            payload=self.aes.desencriptar(payload)
        if self.rx != None:
            self.set_mode(MODE.SLEEP)
            self.reset_ptr_rx()
            BOARD.led_off()
            self.rx(payload)
            self.set_mode(MODE.RXCONT)
        else:
            print(payload)
            self.set_mode(MODE.SLEEP)
            self.reset_ptr_rx()
            BOARD.led_off()
            self.set_mode(MODE.RXCONT)

    def println(self, payload):
        BOARD.led_on()
        while self.get_irq_flags()['tx_done']:
            pass
        if self.aes!=False:
            payload=self.aes.encriptar(payload)
        payload = list(str(payload).encode())
        self.write_payload(payload)
        self.set_mode(MODE.TX)
        BOARD.led_off()


    def on_tx_done(self):
        """Este metodo se llama cada vez que se hace una transmición exitosa
           de un mensaje"""
        self.set_mode(MODE.STDBY)
        self.clear_irq_flags(tx_done=1)
