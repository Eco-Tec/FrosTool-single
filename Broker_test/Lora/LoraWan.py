#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 11 21:11:11 2018

@author: game
"""
from Lora.Lora import Lora
import time
from .cryp import cryp
from config import list_id
from machine import Timer

class Gateway(Lora):
    list_device={}
    def __init__(self,callrx=None,passw=None,passw_id=None):
        Lora.__init__(self,Modo='Rx',callrx=self.broker_rx,passw=passw)
        self.back=callrx
        self.id=id
        self.Timer = Timer(0)              # create a timer object using timer 4
        self.Timer.init(period=1000,mode=Timer.PERIODIC,callback=self.refresh_device)                # trigger at 2Hz
        self.Timewindows = 10
        if passw_id !=None:
            try:
                self.aes_2=cryp(passw_id)
            except:
                self.aes_2=False

        #print(self.aes.encriptar('FrosTool 00001'))
        #print(self.aes.encriptar('FrosTool 00002'))

    def refresh_device(self,timer):
        """Metodo para deteminar cuanto tiempo a transcurrido desde la ultima
        transmiccion de un dispositivo y si este a superado la ventana de espera
        se saca de la lista"""
        #print('timer_test')
        #print(self.list_device)
        for id in self.list_device:
            self.list_device[id]+=1
            if self.list_device[id]>=self.Timewindows:
                del self.list_device[id]

    def delet_device(self,id):
        del self.list_device[id]

    def search_id(self,name,id):
        """Busca si el id de un dispositivo corresponde a uno aceptado para usarce
        en la red y lo agrega al diccionario de control de la ventana de tiempos"""
        for i in list_id:
            if i==name:
                self.list_device[id]=0
                return True
        return False

    def search_device(self,id):
        """Busca si el dispositivo esta en el diccionario de las ventanas de tiempos
        y de esta manera determinar si se aceptan datos o debe volver a identificarce"""
        if id in self.list_device:
            self.list_device[id]=0
            return True
        else:
            return False

    def broker_rx(self,msm):
        """Metodo utilizado para gestionar los datos recividos por broker
        verifica que el paquete recivido contenga un id valida, para responderle
        de manera afimativa al cliente y retorna el payload al programa que lo solicite"""
        #self.set_RxTx('Tx')
        a=self.read_packet(msm)
        for i,b in a.items():
            print(i)
            print(b)
        """
        if a['counter']=='0':
            name = self.aes.desencriptar(a['id'])   #desencripta el id
            if self.search_id(name,a['id']): #Solicita si el id esta en la lista de dispositivo permidits
                pack=self.crear_packet(a['id'], 'True', a['counter']) #REsponde que la solicitud de conexion es aceptada
                self.send(pack)
                self.set_RxTx('Rx')
            else: #El id del dispositivo no es aceptado para conectarce
                pack=self.crear_packet(a['id'], 'Fasle', a['counter'])
                self.send(pack)
                self.set_RxTx('Rx')
        elif a['counter']<='99': #Comprueba que el numero de paquetes transmitidos por el dispositivo no excede los 99
            if self.search_device(a['id']): #Verifica que id del dispositivo no exceda el tiempo de conexion
                pack=self.crear_packet(a['id'], 'True', a['counter'])
                self.send(pack)
                self.set_RxTx('Rx')
                if a['payload']==False: # Verifica si el dispositivo solicita su desconexion
                    self.delet_device(a['id'])
                else: #Si no llama la funcion solicitada
                    self.back(a['payload'])
            else: #Si el dispositivo supero la ventana de espera se le informa para que vuelva autenticarce
                pack=self.crear_packet(a['id'], 'Fasle', 0)
                self.send(pack)
                self.set_RxTx('Rx')
        else:# Si el numero de paquetes es mayor a 99 se desactiva el dispositivo para que vuelva autenticar
            if self.search_device(a['id']):
                self.delet_device(a['id'])
                pack=self.crear_packet(a['id'], 'Fasle', 0)
                self.send(pack)
                self.set_RxTx('Rx')
                """

    def crear_id(self,id):
        a=len(id)
        if a<=16:
            c=16-a
            id = id+" "*c
            return id
        else:
            print('Id supera los 16 caracteres')

    def crear_payload(self,payload):
        a=len(payload)
        if a<=112:
            c=112-a
            payload = payload+' '*c
            return payload
        else:
            print('Payload supera la loguitud')

    def crear_counter(self,counter):
        counter=str(counter)
        a = len(counter)
        if a<=3:
            c=3-a
            counter = (' '*c)+counter
            return counter
        else:
            print('El contador de paquetes excede la longuitud')

    def crear_packet(self,id,msm,counter):
        id=self.crear_id(id)
        payload=self.crear_payload(msm)
        counter =self.crear_counter(counter)
        return id+payload+counter

    def read_packet(self,msm):
        a=len(msm)
        b=a-3
        id = msm[0:16]
        payload = msm[16:b]
        counter = msm[b:]
        return {'id':id,'payload':payload,'counter':counter}

    def send(self,pack):
        #print(pack)
        self.println(pack)
