#import gc
#import micropython
#micropython.mem_info()
#print('memoria al iniciar')
from Lora.Lora import Lora
#micropython.mem_info()
#print('lora importado')
from MQTT import MQTT
#micropython.mem_info()
#print('mqtt importado')
from cultivo import Cultivo
#micropython.mem_info()
#print('cultivo importado')
from debug import debug_mode
#micropython.mem_info()
#print('debug importado')

#from Lora.Register import*

from gps import *


def on_rx(payload):
    print(payload)


def main():
    debug=debug_mode(True)
    mqtt = MQTT(debug)
    gps=GpsNeo6(2,9600,diff=2)
    cultivo = Cultivo(debug, mqtt)
    lora = Lora(verbose=False)
    #print(lora)
    while True:
        cultivo.read_sensores()
        gps.get_time()
        print(gps.hora)  
        gps.get_date()
        print(gps.date)
        gps.get_altura()
        print(gps.altitud)
        gps.get_posicion()
        print(gps.latitud)
        print(gps.longitud)
        time.sleep(30)

if __name__ == '__main__':
    main()
